<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class DashboardController extends Controller
{

	/**
	*@Route("/dashboard",name="dashboard")
	*
	*/

	public function showAction(Request $request){

		$usersList 		=	$this->getDoctrine()
							->getRepository(User::class)
							->findBy(array('role' => 'ROLE_USER'));

		return $this->render('default/index.html.twig',compact('usersList'));

	}


    /**
     * Deletes a Post entity.
     *
     * @Route("/user/{id}/delete", name="admin_user_delete")
     * @Method("GET")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function deleteAction(Request $request, User $user)
    {
        
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        $this->addFlash('success', 'User deleted successfully');

        return $this->redirectToRoute('dashboard');

    }
}
