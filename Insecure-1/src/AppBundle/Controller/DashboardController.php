<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
class DashboardController extends Controller
{

	/**
	*@Route("/dashboard",name="dashboard")
	*
	*/

	public function showAction(Request $request){

		$usersList 		=	$this->getDoctrine()
                             ->getRepository(User::class)
                             ->findBy(array('role' => 'ROLE_USER'));
     $custom_token      =   md5(uniqid(rand(), true));

     $cookieGuest = array(
        'name'  => 'cust_token',
        'value' =>  $custom_token,
        'path'  => '',
        'time'  => time() + 3600
        );

     $cookie = new Cookie($cookieGuest['name'], $cookieGuest['value'], $cookieGuest['time'], $cookieGuest['path']);

     $response = new Response();
     $response->headers->setCookie($cookie);
     $response->send();
     return $this->render('default/index.html.twig',compact('usersList','custom_token'));

 }


    /**
     * Deletes a Post entity.
     *
     * @Route("/user/{id}/delete", name="admin_user_delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request, User $user)
    {

        if($request->request->get('_token') == $_COOKIE['cust_token']){             
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();

            $this->addFlash('success', 'User deleted successfully');
        }else{

            $this->addFlash('failure','Token mismatch');

        }
        return $this->redirectToRoute('dashboard');
    }
}
