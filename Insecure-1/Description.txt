Sending a random token with the GET request and checking that token against a cookie value at the server side does 
not provide sufficient protection
 against CSRF attack. Since the cookie remains exposed, the attacker can easily manipulate 
the random string available in the cookie and 
craft a request with a token, which will match the manipulated cookie value.
